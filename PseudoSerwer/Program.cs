﻿using System;

namespace PseudoSerwer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Pseudo Server";

            Serwer.Start(5, 26950);

            Console.ReadKey();
        }
    }
}
