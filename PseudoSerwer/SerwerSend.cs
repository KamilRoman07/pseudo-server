﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PseudoSerwer
{
    class SerwerSend
    {
        private static void SendTCPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            Serwer.clients[_toClient].tcp.SendData(_packet);
        }

        private static void SendTCPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Serwer.MaxPlayers; i++)
            {
                Serwer.clients[i].tcp.SendData(_packet);
            }
        }
        private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Serwer.MaxPlayers; i++)
            {
                if (i != _exceptClient)
                {
                    Serwer.clients[i].tcp.SendData(_packet);
                }
            }
        }

        public static void Message(int _toClient, string _msg)
        {
            using (Packet _packet = new Packet((int)ServerPackets.welcome))
            {
                _packet.Write(_msg);
                _packet.Write(_toClient);

                SendTCPData(_toClient, _packet);
            }
        }

        public static void CoordsPacket(int _toClient, string _coords)
        {
            using (Packet _packet = new Packet((int)ServerPackets.coordsPacket))
            {
                _packet.Write(_coords);
                _packet.Write(_toClient);

                SendTCPData(_toClient, _packet);
            }
        }
    }
}
